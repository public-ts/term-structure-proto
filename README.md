# proto file for term-structure-proto

a proto for grpc

## rules for version upgrade

```
${MAJOR-version}.${MINOR-version}.${PATCH-version}
```

* MAJOR version when you make incompatible API changes
* MINOR version when you add functionality in a backwards compatible manner
* PATCH version when you make backwards compatible bug fixes
